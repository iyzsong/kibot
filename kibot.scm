(use-modules
 (rnrs io ports)
 (srfi srfi-41)
 (srfi srfi-19)
 (ice-9 regex)
 (ice-9 format)
 (ice-9 rdelim)
 (ice-9 expect)
 (ice-9 q))

(define *irc-host* (or (getenv "IRC_HOST") "127.0.0.1"))
(define *irc-port* (or (and=> (getenv "IRC_PORT") string->number) 6667))
(define *irc-user* (or (getenv "IRC_USER") "kibot"))
(define *irc-pass* (getenv "IRC_PASS"))
(define *irc-channel* (getenv "IRC_CHANNEL"))
(define %q (make-q))

(define serenity-prayer
  "God grant me the serenity
To accept the things I cannot change;
Courage to change the things I can;
And wisdom to know the difference.
Living one day at a time;
Enjoying one moment at a time;
Accepting hardships as the pathway to peace;
Taking, as He did, this sinful world
As it is, not as I would have it;
Trusting that He will make things right
If I surrender to His Will;
So that I may be reasonably happy in this life
And supremely happy with Him
Forever and ever in the next.
Amen.")
(define %last-prayed #f)

(define (open-irc-socket host port)
  (let ((sock (socket PF_INET SOCK_STREAM 0)))
    (connect sock AF_INET (inet-pton AF_INET host) port)
    (setvbuf sock 'line)
    sock))

(define (irc-receive-message sock)
  (let* ((line (get-line sock))
         (m (if (eof-object? line)
                line
                (string-trim-right line #\cr))))
    (format #t "< ~a\n" m)
    m))

(define irc-message-valid?
  (let ((rx (make-regexp "^(PONG|NOTICE|USER|NICK|PASS|JOIN) [[:print:]\t]{1,500}$")))
    (lambda (message)
      (regexp-exec rx message))))

(define (irc-send-message sock message)
  (if (irc-message-valid? message)
      (begin
        (format sock "~a\r\n" message)
        (format #t "> ~a\n" message))
      (format #t "! INVALID_MESSAGE: ~s\n" message)))

(define (irc-handle-message message)
  (with-input-from-string message
    (lambda ()
      (expect-strings
       ("^PING :(.+)$" =>
        (lambda (_ server)
          (enq! %q (format #f "PONG ~a" server))))

       ("^:([^ ]+ PRIVMSG (#[^ ]+) :PRAY$)" =>
        (lambda (_ prefix channel)
          (when (string=? channel *irc-channel*)
            (let ((now (current-time)))
              (if (or (not %last-prayed)
                      (> (time-second (time-difference now %last-prayed))
                         86400))
                  (begin
                    (set! %last-prayed now)
                    (for-each
                     (lambda (s)
                       (enq! %q (format #f "NOTICE ~a :~a" channel s)))
                     (string-split serenity-prayer #\newline)))
                  (enq! %q (format #f "NOTICE ~a :~a" channel
                                   "Grace never runs out, but come to me next day!")))))))))))

(let ((sock (open-irc-socket *irc-host* *irc-port*)))
  (when *irc-pass*
    (irc-send-message sock (format #f "PASS ~a" *irc-pass*)))
  (irc-send-message sock (format #f "NICK ~a" *irc-user*))
  (irc-send-message sock (format #f "USER ~a 0 * kibot" *irc-user*))
  (when *irc-channel*
    (irc-send-message sock (format #f "JOIN ~a" *irc-channel*)))

  (while #t
    (while (not (q-empty? %q))
      (irc-send-message sock (deq! %q))
      (usleep 2666666))

    (let ((message (irc-receive-message sock)))
      (when (eof-object? message)
        (format #t "! EOF\n")
        (exit 0))
      (irc-handle-message message))))
